import ply.lex as lex
import ply.yacc as yacc

#----- Analyse lexicale -----

tokens = (
    'DEBUT',
    'NOMBRE',
    'ADDITION',
    'SOUSTRACTION',
    'MULTIPLICATION',
    'DIVISION',
    'PARENTHESEG',
    'PARENTHESED',
    'POINT',
    'NOMBREFR'
)

# Expressions rationnelles

t_DEBUT = r'Ordinateur\,\ combien\ font\ \:'
t_ADDITION = r'plus'
t_SOUSTRACTION = r'moins'
t_MULTIPLICATION = r'fois'
t_DIVISION = r'divise-par'
t_PARENTHESEG = r'entre-parentheses'
t_PARENTHESED = r'\,'
t_POINT = r'\?'

nombres = {"un":1, "deux":2, "trois":3, "quatre":4, "cinq":5, "six":6, "sept":7, 'huit':8, 'neuf':9}

def t_NOMBREFR(t):
    t.value = nombres[t.value]
    return t

t_NOMBREFR.__doc__="|".join(list(nombres.keys()))

def t_NOMBRE(t):
    r'\d+'
    t.value = int(t.value)
    return t

t_ignore = ' \t\n'

def t_error(t):
    print("ZUT! Votre langage n'est point le mien !")
    print("Je ne connais pas le mot "+t.value[0])
    t.lexer.skip(1)
    
    
lexer = lex.lex()
#lexer.input(source)

#while True:
#    tok = lexer.token()
#    if not tok:
#        break
#    print(tok)

#----- Analyse Syntaxique ------

precedence = (
    ( 'left', 'ADDITION', 'SOUSTRACTION' ), # Plus basse priorité
    ( 'left', 'MULTIPLICATION', 'DIVISION'),
    ( 'nonassoc', 'NEGATIF')                # Plus haute priorité
)


# p = [valeur, symbole1, symbole2...]

def p_phrase(p):
    'expr : DEBUT expr POINT'
    p[0] = p[2]

def p_addition(p):
    'expr : expr ADDITION expr'
    p[0] = p[1] + p[3]

def p_soustraction(p):
    'expr : expr SOUSTRACTION expr'
    p[0] = p[1] - p[3]

def p_negatif(p):
    'expr : SOUSTRACTION expr %prec NEGATIF'
    p[0] = -p[2]

def p_mult_div(p):
    '''expr : expr MULTIPLICATION expr
            | expr DIVISION expr'''
    if p[2] == "fois":
        p[0] = p[1] * p[3]
    else:
        if p[2]!=0:
            p[0] = p[1] / p[3]
        else:
            print("Sapristi ! Vous tentez de diviser par zéro, mécréant !")
            raise ZeroDivisionError("Diviser par zéro ! On aura tout vu !")
            
def p_NOMBRE(p):
    'expr : NOMBRE'
    p[0] = p[1]

def p_NOMBREFR(p):
    'expr : NOMBREFR'
    p[0] = p[1]

def p_paren(p):
    'expr : PARENTHESEG expr PARENTHESED'
    p[0] = p[2]

def p_error(p):
    print("Ce que vous dites n'a aucun sens...")

parser = yacc.yacc()

source = '''
Ordinateur, combien font :
entre-parentheses deux plus trois,
fois moins quatre divise-par deux moins un ?
'''

res = parser.parse(f"\n {source}")
print(source)
print(f"\nCela fait {res} humain.\n\n")